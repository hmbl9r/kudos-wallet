String.prototype.format=function(){
    return(function(a,t){return t.replace(/\{(\d+)\}/g,function(_,i){return a[parseInt(i,10)];});})(arguments,this);
};

(function ($) {
    $.fn.ajaxForm = function(options) {
        if (typeof(options) === 'undefined') {
            options = {
                failed: function() {
                    alert('A network error has occured! Please reload the page.');
                }
            };
        }
        
        var self = this;
        
        self.submit(function(e) {
            e.preventDefault();
            
            if (typeof(options.beforeSubmit) === 'function') {
                _.bind(options.beforeSubmit, self)();
            }
            
            var ajaxFn = self.attr('method').toLowerCase() === 'post' ? $.post : $.get;
            
            var params = _.inject(self.serializeArray(), function(m, o) {
                if (m[o.name] !== undefined) {
                    if (typeof(m[o.name].push) === 'function') {
                        m[o.name].push(o.value);
                    } else {
                        m[o.name] = [o.value, m[o.name]];
                    }
                } else {
                    m[o.name] = o.value;
                }
                
                return m;
            }, {});
            
            if (typeof(options.paramsReady) === 'function') {
                params = _.bind(options.paramsReady, self)(params);
            }
            
            ajaxFn(self.attr('action'), params, function(d) {
                if (typeof(options.completed) === 'function') {
                    _.bind(options.completed, self)(d);
                }
            }, 'json').fail(function() {
                _.bind(options.failed, self)();
            });
            
            if (typeof(options.submitted) === 'function') {
                _.bind(options.submitted, self)();
            }
        });
    };
})(jQuery);

$(function() {
    setInterval(function() {
        $.get('/updates', function(d) {
            if (d !== null) {
                $(window).trigger(d.name, d.data);
            }
        }, 'json');
    }, 7000);
});

