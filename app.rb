require 'sinatra'
require 'securerandom'
require 'digest/sha2'
require 'ostruct'
require 'json'
require 'less'
require 'mongoid'
require 'mongoid-history'
require 'net/http'
require 'mail'
require 'uri'
require 'redis'
require 'pry'
require 'rack/webconsole'

I18n.enforce_available_locales = false

Mongoid.load!('./mongoid.yml')

SESSION_LENGTH = 60 * 60 * 24

$redis = Redis.new

configure do
    set :server, :thin
    set :bind, ENV['IP']
    set :port, ENV['PORT'].to_i
    set :public_folder, 'public/'
    set connections: []
    use Rack::Session::Pool, expire_after: SESSION_LENGTH
    use Rack::Webconsole
end

['helper', 'model', 'route'].each do |f|
   Dir["./#{f}s/**/*.#{f}.rb"].each { |i| require i } 
end