class User
    include Mongoid::Document
    include Mongoid::Timestamps
    
    include Mongoid::History::Trackable
    
    field :email, type: String
    
    field :verification, type: String, default: -> { SecureRandom.hex(11) }
    field :verified, type: Boolean, default: false
    
    field :password, type: String
    field :salt, type: String, default: ''
    
    field :balance, type: Float, default: 0.0
    field :unconfirmed_balance, type: Float, default: 0.0
    
    field :address, type: String, default: -> { KudosRPC.new.getnewaddress }
    
    has_many :transactions
    
    validates :email, uniqueness: { message: 'A user with this email already exists.' }
    validates :password, length: { on: :create, minimum: 8, maximum: 32, message: 'Password must be between 8 and 32 characters long.' }
    
    def self.check_login email, pass
        if user = User.where(email: email).first
            entered_pass = Digest::SHA2.hexdigest(pass + user.salt + pass + user.salt)
            
            user if user.password == entered_pass
        end
    end
    
    def recalculate_balances
        kudos = KudosRPC.new
        
        self.transactions.where(:confirmations.lt => 6).each do |tx|
            txinfo = kudos.gettransaction(tx.txid).to_ostruct
            
            tx.confirmations = txinfo.confirmations
            tx.save
        end
                    
        b = self.transactions.where(:confirmations.gte => 6, category: 'deposit').sum(:amount)
        b += self.transactions.where(:category.in => ['usertransfer', 'withdraw']).sum(:amount)
        ub = self.transactions.where(:confirmations.lt => 6, category: 'deposit').sum(:amount)
        
        self.update_attributes(balance: b, unconfirmed_balance: ub)
    end
    
    set_callback(:create, :before) do |d|
        pass = d.password
        salt = Digest::SHA2.hexdigest(SecureRandom.random_bytes + 'im 13, and im so random!')
        
        d.password = Digest::SHA2.hexdigest(pass + salt + pass + salt)
        d.salt = salt
    end
    
    set_callback(:save, :before) do |d|
        (d.attributes.keys - fields.keys).each { |f| d.unset(f) }
    end
end 