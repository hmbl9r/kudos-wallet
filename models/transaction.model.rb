class Transaction
    include Mongoid::Document
    include Mongoid::Timestamps
    
    include Mongoid::History::Trackable
    
    field :txid, type: String
    field :address, type: String
    field :category, type: String
    field :amount, type: Float
    field :fee, type: Float, default: 0.0
    field :confirmations, type: Integer, default: 0
    field :acknowledged, type: Boolean, default: false
    
    belongs_to :user
    
    validates :txid, uniqueness: true
    
    set_callback(:save, :before) do |d|
        (d.attributes.keys - fields.keys).each { |f| d.unset(f) }
    end
end 