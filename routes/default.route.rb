get '/style/:file.css' do
	file = params[:file]

	halt 404 unless File.exists? "./views/less/#{file}.less"

	less "less/#{file}".intern
end

get '/' do
    erb :index
end

get '/updates', require: :login do
    $redis.lpop(session[:user])
end