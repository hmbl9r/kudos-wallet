get '/user' do
    
end

get '/signup' do
    redirect to('/account') if session[:user]
    
    erb :'user/signup'
end

post '/user/signup' do
    out = {}
    user = User.create(params[:user])
    
    if user.valid?
        send_email(params[:user][:email], 'KudosTools Wallet Registration', :signup, { confirm_code: user.verification })
        
        session[:user] = user._id
        out[:success] = true
    else
        out[:success] = false
        out[:errors] = user.errors
    end
    
    out.to_json
end

get '/confirm/:confirmation' do
    user = User.where(verification: params[:confirmation]).first
    
    if user
        user.update_attributes(verified: true, verification: '')
        
        erb :'user/verify_success'
    else
        erb :'user/verify_fail'
    end
end

get '/login' do
    redirect to('/account') if session[:user]
    erb :'user/login'
end

post '/user/login' do
    out = {}
    
    if user = User.check_login(params[:user][:email], params[:user][:password])
        session[:user] = user._id
        out[:success] = true
    else
        out[:success] = false
    end
    
    out.to_json
end

get '/account', require: :login do
    @user = User.find(session[:user])
    @user.recalculate_balances
    
    erb :'user/account'
end

get '/transactions', require: :login do
    @user = User.find(session[:user])
    @user.recalculate_balances
    
    erb :'user/transactions'
end