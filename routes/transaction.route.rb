get '/walletnotify/:txid' do
    pass unless request.ip == '127.0.0.1'
    
    session[:updates] ||= []
    
    kudos = KudosRPC.new
    
    txid = params[:txid]
	tx = kudos.gettransaction(txid).to_ostruct
	details = tx.details.first
	
    user = User.where(address: details.address).first
    
	pass unless details.category == 'receive'
	pass unless user
	
    user.transactions << Transaction.new(
        txid: txid,
        address: details.address,
        category: 'deposit',
        amount: details.amount,
        confirmations: tx.confirmations
    )
    
    user.recalculate_balances
    $redis.rpush(user.id, { name: 'new-balances', data: { balance: user.balance, unconfirmed_balance: user.unconfirmed_balance } }.to_json)
    
	204
end

get '/blocknotify/:bid' do
    kudos = KudosRPC.new
    
    bid = kudos.getblock(params[:bid]).to_ostruct
    
    bid.tx.each do |tx|
        kudos.decoderawtransaction(kudos.getrawtransaction(tx)).to_ostruct.vout.each do |vout|
            vout.scriptPubKey.addresses.each do |addr|
                if user = User.where(address: addr).first
                    user.recalculate_balances
                    $redis.rpush(user.id, { name: 'new-balances', data: { balance: user.balance, unconfirmed_balance: user.unconfirmed_balance } }.to_json)
                end
            end
        end
    end
    
    204
end