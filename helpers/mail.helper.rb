def send_email recipient, subj, templ, context = {}
    Mail.deliver do
        to recipient
        from 'KudosTools Server <kudos@kudostools.org>'
        subject subj
        
        html_part do
            content_type 'text/html; charset=UTF-8'
            
            b = OpenStruct.new(context).instance_eval { binding }
            
            body(ERB.new(File.read("views/emails/#{templ.to_s}.erb")).result(b))
        end
    end
end