class KudosRPC
    class JSONRPCError < RuntimeError; end
 
    def initialize
        @uri = URI('http://u:p@127.0.0.1:9765')
    end
 
    def method_missing(name, *args)
        post_body = { method: name, params: args, id: 'jsonrpc' }.to_json
 
        resp = JSON.parse(http_post_request(post_body))
 
        raise JSONRPCError, resp['error'] if resp['error']
 
        resp['result']
    end
 
    def http_post_request(post_body)
        Net::HTTP.start(@uri.host, @uri.port) do |http|
            request = Net::HTTP::Post.new(@uri.request_uri)
 
            request.basic_auth(@uri.user, @uri.password)
            request.content_type = 'application/json'
            request.body = post_body
 
            http.request(request).body
        end
    end
end