set(:require) do |what|
    condition do
        case what
        when :login
            redirect to('/login') if session[:user].nil?
        end
    end
end